# system modules
import argparse
import logging
import sys
import os
import time

# internal modules
from umphy_control_center.app import UmphyControlCenter, run

# external modules

parser = argparse.ArgumentParser(
    prog="python -m umphy_control_center"
    if "__main__.py" in sys.argv[0]
    else os.path.basename(sys.argv[0]),
    description=r"""
 _   _                 _              ____            _             _    ____           _            
| | | |_ __ ___  _ __ | |__  _   _   / ___|___  _ __ | |_ _ __ ___ | |  / ___|___ _ __ | |_ ___ _ __ 
| | | | '_ ` _ \| '_ \| '_ \| | | | | |   / _ \| '_ \| __| '__/ _ \| | | |   / _ \ '_ \| __/ _ \ '__|
| |_| | | | | | | |_) | | | | |_| | | |__| (_) | | | | |_| | | (_) | | | |__|  __/ | | | ||  __/ |   
 \___/|_| |_| |_| .__/|_| |_|\__, |  \____\___/|_| |_|\__|_|  \___/|_|  \____\___|_| |_|\__\___|_|   
                |_|          |___/                                                                   

Utilities for our Umphy workgroup
""",
    formatter_class=argparse.RawDescriptionHelpFormatter,
)

logger = logging.getLogger("umphy-control-center")


def cli():
    args = parser.parse_args()
    logging.basicConfig(level=logging.DEBUG)
    run()


if __name__ == "__main__":
    cli()
