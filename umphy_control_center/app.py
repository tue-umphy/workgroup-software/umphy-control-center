# internal modules
try:
    from importlib.resources import files as importlib_resources_files
except ImportError:
    from importlib_resources import files as importlib_resources_files
import abc
import webbrowser
import subprocess
import shutil
import sys
import functools

# external modules
import textual
from textual import events
from textual import log
from textual.screen import Screen
from textual.app import App, ComposeResult
from textual.containers import Container, Vertical, Horizontal
from textual.widgets import Footer, Header, Static, Button
from textual.widgets import Placeholder
import rich
from rich.console import Console
from rich.panel import Panel
from rich.align import Align


class MainMenu(Static):
    def compose(self) -> ComposeResult:
        yield Button("What is this?", id="main-help-button")
        if shutil.which("pacman-mirrors"):
            yield Button(
                "Fix Update Issues", id="main-fix-update-issues-button"
            )
        yield Button("Quit", id="main-quit")

    def on_button_pressed(self, event: Button.Pressed) -> None:
        if event.button.id == "main-help-button":
            self.app.content.replace(HelpPage())
        elif event.button.id == "main-fix-update-issues-button":
            self.app.content.replace(UpdateIssuesPage())
        elif event.button.id == "main-quit":
            self.app.exit()


class UpdateIssuesPage(Static):
    def compose(self) -> ComposeResult:
        if shutil.which("pacman-mirrors"):
            yield Button(
                "Update Mirrors: Run [code]sudo pacman-mirrors -f[/code]",
                id="run-pacman-mirrors-button",
            )

    def on_button_pressed(self, event: Button.Pressed) -> None:
        if event.button.id == "run-pacman-mirrors-button":
            self.app.exit("run-pacman-mirrors")


class HelpPage(Static):
    def compose(self) -> ComposeResult:
        yield Static(
            """
This is the Umphy Control Center.

[@click=open_link('https://gitlab.com/tue-umphy/workgroup-software/umphy-control-center')]https://gitlab.com/tue-umphy/workgroup-software/umphy-control-center[/]
""",
            id="help-page",
        )


class Content(Vertical):
    def __init__(self, *args, **kwargs):
        Container.__init__(self, *args, **kwargs)
        self.history = list()

    def replace(self, content):
        for child in self.children:
            log("Hiding ", child)
            child.styles.display = "none"
        log("Mounting ", content)
        c = (
            Vertical(content, Button("Back", id="button-back"))
            if self.history
            else content
        )
        self.mount(c)
        self.history.append(c)
        log("Content History:", self.history)

    def back(self):
        if self.history:
            last = self.history.pop(-1)
            last.styles.display = "none"
            last.remove()
            log("Content History:", self.history)
        if self.history:
            self.history[-1].styles.display = "block"
        else:
            self.replace(MainMenu())

    def on_button_pressed(self, event: Button.Pressed) -> None:
        if event.button.id == "button-back":
            self.back()


class UmphyControlCenter(App):

    CSS_PATH = str(
        importlib_resources_files("umphy_control_center").joinpath(
            "umphy-control-center.css"
        )
    )

    BINDINGS = [
        ("q", "quit", "Quit"),
        ("escape", "quit", "Quit"),
        ("backspace", "back()", "Back"),
    ]

    async def action_open_link(self, link) -> None:
        log(f"Clicked link ", link)
        xdg_open = shutil.which("xdg-open")
        if xdg_open:
            log(f"Using xdg-open to open link ", link)
            subprocess.check_call(
                [xdg_open, link],
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL,
                stdin=subprocess.DEVNULL,
            )
        else:
            log(f"Opening link ", link, " in webbrowser")
            webbrowser.open(link)

    def action_back(self) -> None:
        self.content.back()

    def on_load(self, event):
        self.title = "Umphy Control Center"

    def compose(self) -> ComposeResult:
        yield Header()
        self.content = Content(id="content")
        yield self.content
        yield Footer()

    def on_mount(self):
        self.query_one("#content").replace(MainMenu())


def exit_on_keyboardinterrupt(decorated_fun):
    @functools.wraps(decorated_fun)
    def wrapper(*args, **kwargs):
        try:
            decorated_fun(*args, **kwargs)
        except KeyboardInterrupt:
            sys.exit(0)

    return wrapper


@exit_on_keyboardinterrupt
def run():
    console = Console()
    while True:
        console.log("Launching app")
        app = UmphyControlCenter(watch_css=True)
        result = app.run()
        console.log("App exited with ", result)
        if result is None:
            console.log("User wants to quit")
            sys.exit(0)
        elif result == "run-pacman-mirrors":
            cmd = ["sudo", "pacman-mirrors", "-f"]
            console.log(f"Run [bold]{' '.join(cmd)}[/bold]")
            try:
                subprocess.check_call(cmd)
            except Exception as e:
                console.log("Error: ", e)
            input("Press [ENTER] to continue")
        else:
            console.log(f"Unknown result ", result)


if __name__ == "__main__":
    run()
