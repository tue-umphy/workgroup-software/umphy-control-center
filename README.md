# ⚙️  Umphy Control Center

Utilities for our workgroup.

## ✨ Features

- nothing yet...

## 📥 Installation

```bash
# Install as a normal Python package
pip install git+https://gitlab.com/tue-umphy/workgroup-software/umphy-control-center
```

If you have our [Arch/Manjaro Workgroup Repository](https://gitlab.com/tue-umphy/workgroup-software/repository) set up, you probably already have it installed. Otherwise, install the `umphy-control-center` package (e.g. `sudo pacman -Syu umphy-control-center` or use your GUI software installer of choice).

## ❓ Usage

```bash
# Launch the control center
umphy-control-center

# If that doesn't work try:
python -m umphy_control_center
```

A desktop file for the Umphy Control Center is also installed, browse your installed programs for _„Umphy”_.
